package com.vdroog1.ludumdare31;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Created by kettricken on 07.12.2014.
 */
public class Ashes {

    private static final long LIFETIME = 1000 * 10;

    Rectangle rectangle;

    long timestamp;

    public Ashes(Rectangle rectangle) {
        timestamp = TimeUtils.millis();
        this.rectangle = rectangle;
    }

    public boolean isValid(){
        if (TimeUtils.millis() - timestamp > LIFETIME)
            return false;
        return true;
    }
}
