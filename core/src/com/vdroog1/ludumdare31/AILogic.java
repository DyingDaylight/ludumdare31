package com.vdroog1.ludumdare31;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by kettricken on 07.12.2014.
 */
public class AILogic {

    public static final float MIN_ENERGY_LEVEL = 2;
    public static final float MAX_COLD_LEVEL = 8;
    //TODO
    public static final Vector2 TREE_POSITION = new Vector2(57, 30);
    public static final Rectangle TREE_RECTANGLE = new Rectangle(61, 23, 7, 20);
    public static final float TASK_TIME = 5f;

    Array<Player> bots;
    Array<Fire> fires;
    Array<Food> foods;
    Util util;

    Pixmap pixmap;

    Array<Player> contagiousPlayers = new Array<Player>();
    Vector2 center = new Vector2(GameScreen.WORLD_WITH / 2, GameScreen.WORLD_HEIGHT / 2);

    public AILogic(Array<Player> bots, Array<Fire> fires, Array<Food> foods, Util util, Pixmap pixmap) {
        this.bots = bots;
        this.fires = fires;
        this.foods = foods;
        this.util = util;
        this.pixmap = pixmap;
    }

    public void update(float delta) {
        contagiousPlayers.clear();
        for (Player player : bots) {
            if (player.isContagious)
                contagiousPlayers.add(player);
        }

        for (Player player : bots) {
            if (!player.isBot /*|| player.isContagious*/)
                continue;

          /*  if (player.isWarming && player.cold == 0) {
                Gdx.app.log("test", "isWarm");
                player.isWarming = false;
            }

            if (player.setTreeOnFire &&
                    Intersector.intersectRectangles(player.getWarmingRectangle(), TREE_RECTANGLE, new Rectangle())) {
                Gdx.app.log("test", "setTreeOnFire");
                player.startFire((int) TREE_POSITION.x, (int) TREE_POSITION.y);
                player.setTreeOnFire = false;
            }

            Player contagiousPlayer = null;
            for (Player p : contagiousPlayers) {
                if (!p.equals(player)) {
                    contagiousPlayer = p;
                    break;
                }
            }

            if (player.cold > MAX_COLD_LEVEL && fires.size > 0 && !player.isWarming && !player.setTreeOnFire) {
                Gdx.app.log("test", "run to fire");
                player.ripleyPoint.x = fires.get(0).rectangle.x;
                player.ripleyPoint.y = fires.get(0).rectangle.y;
                player.isWarming = true;
                boolean hasDestination = player.findPath(pixmap, (int) player.ripleyPoint.x, (int) player.ripleyPoint.y);
                if (!hasDestination) {
                    player.isWarming = false;
                }
                player.taskTime = 0;
            } else if (player.cold > MAX_COLD_LEVEL && fires.size == 0 && !player.isWarming
                    && !player.setTreeOnFire) {
                Gdx.app.log("test", "run to start a fire");
                player.ripleyPoint.x = TREE_POSITION.x;
                player.ripleyPoint.y = TREE_POSITION.y;
                player.setTreeOnFire = true;
                boolean hasDestination = player.findPath(pixmap, (int) player.ripleyPoint.x, (int) player.ripleyPoint.y);
                if (!hasDestination) {
                    Gdx.app.log("test", "no path to tree");
                    player.setTreeOnFire = false;
                }
                player.taskTime = 0;
            } else if (player.energy < MIN_ENERGY_LEVEL && foods.size > 0 && !player.isEating) {
                Gdx.app.log("test", "run to eat");
                player.ripleyPoint.x = foods.get(0).rectangle.x;
                player.ripleyPoint.y = foods.get(0).rectangle.y;
                player.isEating = true;
                boolean hasDestination = player.findPath(pixmap, (int) player.ripleyPoint.x, (int) player.ripleyPoint.y);
                if (!hasDestination) {
                    player.isEating = false;
                }
                player.taskTime = 0;
            } else if (contagiousPlayer != null) {
                Gdx.app.log("test", "run to contagious");
                player.ripleyPoint.x = contagiousPlayer.rectangle.x;
                player.ripleyPoint.y = contagiousPlayer.rectangle.y;
                player.findPath(pixmap, (int) player.ripleyPoint.x, (int) player.ripleyPoint.y);
                player.taskTime = 0;
            } else */ if (player.taskTime >= TASK_TIME /*&& !player.isWarming && !player.setTreeOnFire  && !player.isEating*/) {
                getNewDestinationPoint(player);

                player.taskTime = 0;
                boolean hasDestination = player.findPath(pixmap, (int) player.ripleyPoint.x, (int) player.ripleyPoint.y);
                if (!hasDestination) {
                    getNewDestinationPoint(player);
                    player.findPath(pixmap, (int) player.ripleyPoint.x, (int) player.ripleyPoint.y);
                }
            }

            if (player.path.isEmpty()) {
                return;
            }

            int destXGrid = player.path.get(player.path.size() - 2);
            int destYGrid = player.path.get(player.path.size() - 1);

            int destY = player.getGridY(player.y);
            int destX = player.getGridX(player.x);
            if (destXGrid > destX){
                player.speedX = Player.playerSpeed;
                player.speedY = 0;
            } else if (destXGrid < destX){
                player.speedX = Player.playerSpeed * -1;
                player.speedY = 0;
            } else if (destYGrid <destY) {
                player.speedY = Player.playerSpeed * -1;
                player.speedX = 0;
            } else if (destYGrid > destY) {
                player.speedY = Player.playerSpeed;
                player.speedX = 0;
            }
            destX = player.getGridX(player.x + player.speedX);
            destY = player.getGridY(player.y + player.speedY);
            if (destX == destXGrid && destY == destYGrid) {
                player.path.remove(player.path.size() - 2);
                player.path.remove(player.path.size() - 1);
            }
            player.move();

            if (player.path.isEmpty()) {
                player.startFire((int) player.ripleyPoint.x, (int) player.ripleyPoint.y);
            }
        }
    }

    private void getNewDestinationPoint(Player player) {
        int index = MathUtils.random(0, util.ripley.getVertices().length - 1);
        float rX, rY;
        if (index % 2 == 0) {
            rX = util.ripley.getVertices()[index];
            rY = util.ripley.getVertices()[index + 1];
        } else {
            rX = util.ripley.getVertices()[index - 1];
            rY = util.ripley.getVertices()[index];
        }

        int d = 2;
        float diff = (float) Math.sqrt(Math.pow(rX - center.x, 2) + Math.pow(rY - center.y, 2));
        player.ripleyPoint.x = Math.max(0, rX + d / diff * (center.x - rX));
        player.ripleyPoint.y = Math.max(0, rY + d / diff * (center.y - rY));
    }

    Vector2 point = new Vector2();
    public void setTestPoint(int x, int y) {
        point.x = x;
        point.y = y;
        for (Player player1 : bots) {
            player1.findPath(pixmap, x, y);
        }
    }
}
