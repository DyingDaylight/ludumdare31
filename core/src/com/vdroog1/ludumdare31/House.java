package com.vdroog1.ludumdare31;

import com.badlogic.gdx.math.Rectangle;

/**
 * Created by kettricken on 07.12.2014.
 */
public class House {

    Rectangle rectangle;
    boolean isOnFire = false;

    public House() {
        rectangle = new Rectangle(264 + Constants.mapOffset, 176 + Constants.mapOffset, 264, 230);

    }

    public void setOffFire() {
        isOnFire = false;
    }

    public void setOnFire() {
        isOnFire = true;
    }
}
