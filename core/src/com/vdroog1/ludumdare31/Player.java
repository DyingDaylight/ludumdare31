package com.vdroog1.ludumdare31;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ShortArray;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by kettricken on 06.12.2014.
 */
public class Player {

    public static final int WARMING_INTERVAL = 1;
    public static final int playerSpeed = 4;
    public static final int playerHeight = 24;

    private static final long ENERGY_UPDATE = 7 * 1000;
    private static final long COLD_UPDATE = 2 * 1000;
    private static final long WARMING_AREA = 24;
    private static final long PLAYER_WIDTH = 24;
    private static final long PLAYER_HEIGHT = 24;

    private long lastEnergyUpdate;
    private long lastColdUpdate;

    public int x = 320;
    public int y = 100;

    int fireDistance = 50;
    public Vector2 firePoint;
    public Vector2 lastFireAim = new Vector2();
    public Vector2 center = new Vector2();
    public Vector2 flameThrowerPoint = new Vector2(0, 14);

    Array<Vector2> fireTriangle = new Array<Vector2>();

    Rectangle rectangle;
    Rectangle warmingRectangle;

    float energy = 100;
    float cold = 0;
    float life = 100;

    final boolean isBot;

    float warmingTime = 0;
    float onFireTime = 0;
    float contagiousTime = 0;
    float checkTime = 0;

    StatusBar statusBar;
    public boolean isContagious = false;

    float taskTime = 0;

    Vector2 ripleyPoint = new Vector2();
    boolean isWarming = false;
    boolean setTreeOnFire = false;
    boolean isEating = false;

    int cellSize = playerHeight;
    int gridWidth = GameScreen.WORLD_WITH / cellSize + 1;
    int gridHeight = GameScreen.WORLD_HEIGHT / cellSize + 1;
    int[][] weight = new int[gridHeight][gridWidth];

    public int speedX = 0;
    public int speedY = 0;


    Util util;
    Texture texture;

    PolygonRegion polygonRegion;
    TextureRegion textureRegion;

    EarClippingTriangulator earClippingTriangulator = new EarClippingTriangulator();
    float[] vertices = new float[0];
    ShortArray triangles;

    public Player(boolean isBot, Util util) {
        lastEnergyUpdate = TimeUtils.millis();
        lastColdUpdate = TimeUtils.millis();
        fireTriangle.add(new Vector2());
        fireTriangle.add(new Vector2());
        fireTriangle.add(new Vector2());
        this.isBot = isBot;
        this.util = util;
        if (isBot) {
            x = MathUtils.random(264 + Constants.mapOffset, 387 - playerHeight + Constants.mapOffset);
            y = MathUtils.random(104 + Constants.mapOffset, 178 - playerHeight - Constants.mapOffset);
        } else {
        }
        statusBar = new StatusBar();

        if (isBot) {
            taskTime = Float.MAX_VALUE;
            texture = TextureManager.bot;
        } else {
            texture = TextureManager.player;
        }
        textureRegion = new TextureRegion(TextureManager.flameRegion);

    }

    public  Rectangle getRectangle() {
        rectangle = new Rectangle(x, y, PLAYER_WIDTH, PLAYER_HEIGHT);
        return rectangle;
    }

    public void setVirus() {
        isContagious = true;
        contagiousTime = 0;
    }

    public void startFire(int screenX, int screenY) {
        lastFireAim.set(screenX, screenY);
        firePoint = Util.getPointOnLine(new Vector2(screenX, screenY), new Vector2(flameThrowerPoint.x + x, flameThrowerPoint.y + y),
                fireDistance);

        vertices = new float[] {
                flameThrowerPoint.x + x, flameThrowerPoint.y + y,
                firePoint.x - playerHeight / 2, firePoint.y - playerHeight / 2,
                firePoint.x + playerHeight / 2, firePoint.y + playerHeight / 2
        };
        triangles = earClippingTriangulator.computeTriangles(vertices);
        polygonRegion = new PolygonRegion(textureRegion, vertices, triangles.items);
        fireTriangle.get(0).set(flameThrowerPoint.x + x, flameThrowerPoint.y + y);
        fireTriangle.get(1).set(firePoint.x - playerHeight / 2, firePoint.y - playerHeight / 2);
        fireTriangle.get(2).set(firePoint.x + playerHeight / 2, firePoint.y + playerHeight / 2);
    }

    public void stopFire(){
        firePoint = null;
    }

    public void drawFire(PolygonSpriteBatch batch) {
        if (firePoint != null) {
            batch.draw(polygonRegion,  0,0);
            batch.flush();
        }
    }

    public void draw(SpriteBatch batch){
        if (!isBot) {
            batch.setColor(1,1,1,1);
            statusBar.draw(batch);
        }
    }

    public void setX(float x){
        this.x = (int) x;
        startFire();
    }

    public void setY(float y){
        this.y = (int) y;
        startFire();
    }

    private void startFire() {
        if (firePoint != null) {
            startFire((int) lastFireAim.x, (int) lastFireAim.y);
        }
    }

    boolean isOnFire = false;
    public void setOnFire() {
        isOnFire = true;
        onFireTime = 0;
    }

    public void getEnergy(float energy) {
        isEating = false;
        this.energy = Math.min(energy + this.energy, 100);
    }

    public void update(Pixmap pixmap, float delta){
        if (TimeUtils.millis() - lastEnergyUpdate > ENERGY_UPDATE) {
            energy = Math.max(0f, energy - 5f);
            lastEnergyUpdate = TimeUtils.millis();
        }
        if (TimeUtils.millis() - lastColdUpdate > COLD_UPDATE) {
            cold = Math.min(100, cold + 5f);
            lastColdUpdate = TimeUtils.millis();
        }
        statusBar.setEnergyLevel(energy);
        statusBar.setColdLevel(cold);
        statusBar.setLifeLevel(life);

        if (isOnFire)
            onFireTime += delta;
        if (isContagious)
            contagiousTime += delta;

        checkTime += delta;

        if (onFireTime >= 5) {
            isOnFire = false;
            onFireTime = 0;
        }

        if (contagiousTime > 5) {
            isContagious = false;
            contagiousTime = 0;
        }

        if (checkTime >= 1) {
            checkTime = 0;
            if (isContagious)
                life --;
            if (isOnFire)
                life --;
            if (!isContagious && !isOnFire && life < 100)
                life++;
        }

        if (!isBot) {
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                if (testCollision(Input.Keys.UP, new Vector2(x, y + playerSpeed), pixmap)) {
                    setY(y + playerSpeed);
                }
            } else if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                if (testCollision(Input.Keys.DOWN, new Vector2(x, y - playerSpeed), pixmap))
                    setY(y - playerSpeed);
            } else if (Gdx.input.isKeyPressed(Input.Keys.A)) {
                if (testCollision(Input.Keys.LEFT, new Vector2(x - playerSpeed, y), pixmap))
                    setX(x -= playerSpeed);
            } else if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                if (testCollision(Input.Keys.RIGHT, new Vector2(x + playerSpeed, y), pixmap)) {
                    setX(x += playerSpeed);
                }
            }
        } else {
            taskTime += delta;
        }
    }

    public Rectangle getWarmingRectangle() {
        warmingRectangle = new Rectangle(x - WARMING_AREA, y - WARMING_AREA, WARMING_AREA * 2 + playerHeight,
                WARMING_AREA * 2 + playerHeight);
        return warmingRectangle;
    }

    public void warm() {
        cold = Math.max(0, cold - 1f);
    }

    public boolean isOnFire() {
        return isOnFire;
    }

    public boolean isFiring() {
        return firePoint != null;
    }

    public Vector2 getCenter(){
        center.x = x + PLAYER_WIDTH / 2;
        center.y = y + PLAYER_HEIGHT / 2;
        return center;
    }

    public void testPlayerForFire(Array<Fire> fires, float delta) {
        boolean wasWarmed = false;
        Iterator<Fire> fireIterator = fires.iterator();
        boolean checkWarmig = warmingTime >= WARMING_INTERVAL;
        while (fireIterator.hasNext()) {
            Fire fire = fireIterator.next();
            if (fire.isValid()) {
                if (Intersector.intersectRectangles(fire.rectangle, getRectangle(), new Rectangle())) {
                    setOnFire();
                } else if (Intersector.intersectRectangles(fire.rectangle, getWarmingRectangle(), new Rectangle())) {
                    wasWarmed = true;

                    if (checkWarmig) {
                        warm();
                    }
                }
            }
        }
        if(!checkWarmig && wasWarmed ) {
            warmingTime +=delta;
        }
    }

    public void testForFood(Array<Food> foods){
        Iterator<Food> foodIterator = foods.iterator();
        while (foodIterator.hasNext()) {
            Food food = foodIterator.next();
            if (food.isValid()) {
                if (Intersector.intersectRectangles(food.rectangle, getRectangle(), new Rectangle())) {
                    getEnergy(food.energy);
                    foodIterator.remove();
                }
            } else {
                foodIterator.remove();
            }
        }
    }

    Vector2 topLeftPoint = new Vector2();
    Vector2 topRightPoint = new Vector2();
    Vector2 bottomRightPoint = new Vector2();
    Vector2 bottomLeftPoint = new Vector2();

    Color topLeftColor = new Color();
    Color topRightColor = new Color();
    Color bottomRightColor = new Color();
    Color bottomLeftColor = new Color();

    private boolean testCollision(int dir, Vector2 newPlayerPosition, Pixmap pixmap) {
        topLeftPoint.set(newPlayerPosition.x, newPlayerPosition.y + playerHeight - 1);
        topRightPoint.set(newPlayerPosition.x + playerHeight - 1, newPlayerPosition.y + playerHeight - 1);
        bottomRightPoint.set(newPlayerPosition.x + playerHeight - 1, newPlayerPosition.y);
        bottomLeftPoint.set(newPlayerPosition.x, newPlayerPosition.y);

        topLeftColor.set(getColorFromPixmap(pixmap, (int) topLeftPoint.x, (int) topLeftPoint.y));
        topRightColor.set(getColorFromPixmap(pixmap, (int) topRightPoint.x, (int) topRightPoint.y));
        bottomRightColor.set(getColorFromPixmap(pixmap, (int) bottomRightPoint.x, (int) bottomRightPoint.y));
        bottomLeftColor.set(getColorFromPixmap(pixmap, (int) bottomLeftPoint.x, (int) bottomLeftPoint.y));
        switch (dir) {
            case Input.Keys.UP:
                if (topLeftColor.equals(Color.BLACK)){
                    int y = (int) topLeftPoint.y;
                    int x = (int) topLeftPoint.x;
                    while ((topLeftColor.equals(Color.BLACK))) {
                        topLeftColor.set(getColorFromPixmap(pixmap, x, --y));
                    };
                    setY(y - playerHeight + 1);
                    return false;
                } else if (topRightColor.equals(Color.BLACK)){
                    int y = (int) topRightPoint.y;
                    int x = (int) topRightPoint.x;
                    while ((topRightColor.equals(Color.BLACK))) {
                        topRightColor.set(getColorFromPixmap(pixmap, x, --y));
                    };
                    setY(y - playerHeight + 1);
                    return false;
                }
                break;
            case Input.Keys.DOWN:
                if (bottomLeftColor.equals(Color.BLACK)){
                    int y = (int) bottomLeftPoint.y;
                    int x = (int) bottomLeftPoint.x;
                    while ((bottomLeftColor.equals(Color.BLACK))){
                        bottomLeftColor.set(getColorFromPixmap(pixmap, x, ++y));
                    };
                    setY(y);
                    return false;
                } else if (bottomRightColor.equals(Color.BLACK)){
                    int y = (int) bottomRightPoint.y;
                    int x = (int) bottomRightPoint.x;
                    while (bottomRightColor.equals(Color.BLACK)) {
                        bottomRightColor.set(getColorFromPixmap(pixmap, x, ++y));
                    };
                    setY(y);
                    return false;
                }
                break;
            case Input.Keys.LEFT:
                if (topLeftColor.equals(Color.BLACK)){
                    int y = (int) topLeftPoint.y;
                    int x = (int) topLeftPoint.x;
                    while (topLeftColor.equals(Color.BLACK)) {
                        topLeftColor.set(getColorFromPixmap(pixmap, ++x, y));
                    }
                    setX(x);
                    return false;
                } else if (bottomLeftColor.equals(Color.BLACK)){
                    int y = (int) bottomLeftPoint.y;
                    int x = (int) bottomLeftPoint.x;
                    while (bottomLeftColor.equals(Color.BLACK)) {
                        bottomLeftColor.set(getColorFromPixmap(pixmap, ++x, y));
                    }
                    setX(x);
                    return false;
                }
                break;
            case Input.Keys.RIGHT:
                if (topRightColor.equals(Color.BLACK)){
                    int y = (int) topRightPoint.y;
                    int x = (int) topRightPoint.x;
                    while (topRightColor.equals(Color.BLACK)) {
                        topRightColor.set(getColorFromPixmap(pixmap, --x, y));
                    };
                    setX(x - playerHeight + 1);
                    return false;
                } else if (bottomRightColor.equals(Color.BLACK)){
                    int y = (int) bottomRightPoint.y;
                    int x = (int) bottomRightPoint.x;
                    while (bottomRightColor.equals(Color.BLACK)) {
                        bottomRightColor.set(getColorFromPixmap(pixmap, --x, y));
                    }
                    setX(x - playerHeight + 1);
                    return false;
                }
        }
        return true;
    }

    private int getColorFromPixmap(Pixmap pixmap, int x, int y){
        return pixmap.getPixel(x + Constants.mapOffset, y + Constants.mapOffset);
    }

    public void checkForVirus(Util util) {
        topLeftPoint.set(x, y + playerHeight - 1);
        topRightPoint.set(x + playerHeight - 1, y + playerHeight - 1);
        bottomRightPoint.set(x + playerHeight - 1, y);
        bottomLeftPoint.set(x, y);

        topLeftColor .set(util.pix.getPixel((int) topLeftPoint.x, (int) topLeftPoint.y));
        topRightColor.set(util.pix.getPixel((int) topRightPoint.x, (int) topRightPoint.y));
        bottomRightColor.set(util.pix.getPixel((int) bottomRightPoint.x, (int) bottomRightPoint.y));
        bottomLeftColor .set(util.pix.getPixel((int) bottomLeftPoint.x, (int) bottomLeftPoint.y));
        if (topLeftColor.equals(Color.BLACK)
                || topRightColor.equals(Color.BLACK)
                || bottomLeftColor.equals(Color.BLACK)
                || bottomRightColor.equals(Color.BLACK))
            setVirus();

        if ( firePoint != null) {
            util.removeRipley(fireTriangle);
        }
    }

    Color color = new Color();
    public void checkToBurn(Array<Fire> fires, Pixmap pixmap) {
        for (Vector2 vector2 : fireTriangle) {
            color.set(getColorFromPixmap(pixmap, (int) vector2.x, (int) vector2.y));
            if (color.equals(Color.BLACK)) {
                Fire fire = new Fire((int) vector2.x, (int) vector2.y);
                fires.add(fire);
            }

        }
    }

    public void drawNightFires(SpriteBatch batch, TextureRegion see) {
        if (isOnFire()) {
            batch.draw(see, getCenter().x - GameScreen.LIGHT_HALF_SIZE, getCenter().y - GameScreen.LIGHT_HALF_SIZE);
        }
        if (isFiring()) {
            batch.draw(see, firePoint.x - GameScreen.LIGHT_HALF_SIZE, firePoint.y - GameScreen.LIGHT_HALF_SIZE);
        }

    }

    Intersector.MinimumTranslationVector vector = new Intersector.MinimumTranslationVector();
    Rectangle rect = new Rectangle();
    public void chechFriendlyFireAndContect(Array<Player> bots) {
        for (int i = 0; i < bots.size; i++) {
            Player player = bots.get(i);
            if (player.equals(this))
                continue;
            if (player.isFiring()
                    && Intersector.overlapConvexPolygons(getRectangleVertices(), player.getFireTrianglePolygone(), vector)) {
                setOnFire();
            }
            if (player.isContagious
                    && Intersector.intersectRectangles(getRectangle(), player.getRectangle(), rect)) {
                setVirus();
            }
        }
    }

    public float[] getFireTrianglePolygone() {
        float[] points = new float[fireTriangle.size * 2];
        int index = 0;
        for (Vector2 vector : fireTriangle){
            points[index] = vector.x;
            points[index + 1] = vector.y;
            index += 2;
        }
        return points;
    }

    private float[] getRectangleVertices() {
        return new float[] {
                x, y,
                x + playerHeight, y,
                x + playerHeight, y + playerHeight,
                x, y + playerHeight
        };
    }


    ArrayList<Integer> path = new ArrayList<Integer>();
    public boolean findPath(Pixmap pixmap, int x, int y) {
        if (!isBot)
            return false;
        path.clear();
        clearWeights();
        calcWeights(pixmap, getGridX(this.x), getGridY(this.y), 0);
        boolean isSuccess = restorePath(getGridX(x), getGridY(y));
        return isSuccess;


    }

    int[] offsets = new int[] {1, 0, 0, 1, -1, 0, 0, -1};
    private boolean restorePath(int x, int y) {
        if (x < 0 || x >= weight[0].length ||
                y < 0 || y >= weight.length)
            return false;
        int value = weight[y][x];
        int curX = x;
        int curY = y;

        while (value > 0) {
            boolean smallerFound = false;
            for (int i = 0; i < offsets.length; i += 2) {
                int tmpX = curX + offsets[i];
                int tmpY = curY + offsets[i + 1];
                if (tmpX < 0 || tmpX >= weight[0].length ||
                        tmpY < 0 || tmpY >= weight.length)
                    continue;
                if (weight[tmpY][tmpX] < value) {
                    curX = tmpX;
                    curY = tmpY;
                    value = weight[curY][curX];
                    path.add(curX);
                    path.add(curY);
                    smallerFound = true;
                    break;
                }
            }
            if (!smallerFound){
                return false;
            }
         }
        return true;
    }

    private void calcWeights(Pixmap pixmap, int x, int y, int value) {
        if (x < 0 || x >= gridWidth) return;
        if (y < 0 || y >= gridHeight) return;
        if (weight[y][x] <= value) return;
        if (cellContainsObstacle(pixmap, x, y)) return;
        weight[y][x] = value;
        value++;
        calcWeights(pixmap, x+1, y, value);
        calcWeights(pixmap, x-1, y, value);
        calcWeights(pixmap, x, y+1, value);
        calcWeights(pixmap, x, y-1, value);
    }

    private boolean cellContainsObstacle(Pixmap pixmap, int gridX, int gridY) {
        int realX = getScreenX(gridX);
        int realY = getScreenY(gridY);
        topLeftPoint.set(realX, realY + cellSize);
        topRightPoint.set(realX + cellSize, realY + cellSize);
        bottomRightPoint.set(realX+ cellSize, realY);
        bottomLeftPoint.set(realX, realY);

        topLeftColor .set(getColorFromPixmap(pixmap, (int) topLeftPoint.x, (int) topLeftPoint.y));
        topRightColor.set(getColorFromPixmap(pixmap, (int) topRightPoint.x, (int) topRightPoint.y));
        bottomRightColor .set(getColorFromPixmap(pixmap, (int) bottomRightPoint.x, (int) bottomRightPoint.y));
        bottomLeftColor .set(getColorFromPixmap(pixmap, (int) bottomLeftPoint.x, (int) bottomLeftPoint.y));

        if (topLeftColor.equals(Color.BLACK)
                || topRightColor.equals(Color.BLACK)
                || bottomLeftColor.equals(Color.BLACK)
                || bottomRightColor.equals(Color.BLACK))
            return true;

        topLeftColor = new Color(util.pix.getPixel((int) topLeftPoint.x, (int) topLeftPoint.y));
        topRightColor = new Color(util.pix.getPixel((int) topRightPoint.x, (int) topRightPoint.y));
        bottomRightColor = new Color(util.pix.getPixel((int) bottomRightPoint.x, (int) bottomRightPoint.y));
        bottomLeftColor = new Color(util.pix.getPixel((int) bottomLeftPoint.x, (int) bottomLeftPoint.y));

        if (topLeftColor.equals(Color.BLACK)
                || topRightColor.equals(Color.BLACK)
                || bottomLeftColor.equals(Color.BLACK)
                || bottomRightColor.equals(Color.BLACK))
            return true;
        return false;
    }

    private void clearWeights() {
        for (int i = 0; i < weight.length; i++) {
            for (int k = 0; k < weight[i].length; k++){
                weight[i][k] = Integer.MAX_VALUE;
            }
        }
    }

    public int getGridX(int x){
        return  x / cellSize;
    }

    public int getGridY(int y){
        return y / cellSize;
    }

    public int getScreenX(int x){
        return  x * cellSize;
    }

    public int getScreenY(int y){
        return y * cellSize;
    }

    public void move() {
        x = Math.max(0, x + speedX);
        y = Math.max(0, y + speedY);
    }
}
