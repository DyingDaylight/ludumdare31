package com.vdroog1.ludumdare31;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Created by kettricken on 07.12.2014.
 */
public class Food {
    private static final long LIFETIME = 1000 * 30;
    public static final int WIDTH_HEIGHT = 24;

    int x;
    int y;

    public Rectangle rectangle;

    float energy = 5f;

    long timestamp;

    public Food() {
        timestamp = TimeUtils.millis();

        x = MathUtils.random(252, 372 - WIDTH_HEIGHT);
        y = 162 - WIDTH_HEIGHT;

        rectangle = new Rectangle(x, y, WIDTH_HEIGHT, WIDTH_HEIGHT);
    }

    public boolean isValid() {
        if (TimeUtils.millis() - timestamp > LIFETIME)
            return false;
        return true;
    }
}
