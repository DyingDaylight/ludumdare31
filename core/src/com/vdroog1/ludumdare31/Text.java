package com.vdroog1.ludumdare31;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by kettricken on 07.12.2014.
 */
public class Text extends Actor {

    int x;
    int y;

    BitmapFont font;

    String text;

    public Text(int x, int y, String text) {
        font = new BitmapFont();
        font.setColor(0, 0, 1, 1);
        this.text = text;
        this.x = x;
        this.y = y;
    }

    public void draw(Batch batch) {
        batch.begin();
        font.draw(batch, text, x, y);
        batch.end();
    }

    public void updateText(String s) {
        this.text = s;
    }
}
