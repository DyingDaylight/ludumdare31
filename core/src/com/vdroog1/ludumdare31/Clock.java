package com.vdroog1.ludumdare31;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by kettricken on 07.12.2014.
 */
public class Clock extends Actor {

    boolean isDayTime = true;
    float ratio;
    int degree = 360;

    Color dayColor = new Color(1, 1, 0, 1);
    Color nightColor = new Color(0.3f, 0.2f, 0.3f, 1);

    public static final int DAY_NIGHT_TIME = 30;
    public static final int RADIUS = 24;
    float timeOfDay = 0;

    int x;
    int y;

    Text daysLeft;

    int daysPassed = 0;
    int daysRequired = 4;

    public Clock(){
        x = GameScreen.WORLD_WITH / 2;
        y = GameScreen.WORLD_HEIGHT - RADIUS;
        daysLeft = new Text(x + RADIUS, GameScreen.WORLD_HEIGHT - RADIUS, daysPassed + "/" + daysRequired + " days");
    }

    public void act(float delta) {
        super.act(delta);
        timeOfDay += delta;
        if (timeOfDay > DAY_NIGHT_TIME) {
            if (!isDayTime) {
                daysPassed++;
                daysLeft.updateText(daysPassed + "/" + daysRequired + " days");
            }
            isDayTime = !isDayTime;
            timeOfDay = 0;
        }
        ratio = timeOfDay / DAY_NIGHT_TIME;
    }

    public void draw(ShapeRenderer shapes, SpriteBatch batch) {
        shapes.begin(ShapeRenderer.ShapeType.Filled);
        if (isDayTime)
        shapes.setColor(nightColor);
        else
        shapes.setColor(dayColor);
        shapes.circle(x, y, RADIUS);
        if (isDayTime)
        shapes.setColor(dayColor);
        else
            shapes.setColor(nightColor);
        shapes.arc(x, y, RADIUS, 90, degree);
        shapes.end();
        degree = 360 - (int) (360 * ratio);

        daysLeft.draw(batch);
    }

    public void restart() {
        daysPassed = 0;
        timeOfDay = 0;
        isDayTime = true;
    }
}
