package com.vdroog1.ludumdare31;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by kettricken on 07.12.2014.
 */
public class StatusBar extends Actor {

    int width = 100;
    int height = 16;

    Color foodColor = new Color(0, 1, 0, 1);
    Color foodSecColor = new Color(0.13f, 0.7f, 0.3f, 1f);

    Color coldColor = new Color(0.14f, 0.65f, 0.68f, 1);
    Color coldSecColor = new Color(0.18f, 0.13f, 0.68f, 1f);

    Color lifeColor = new Color(1f, 0f, 0f, 1);
    Color lifeSecColor = new Color(0.5f, 0f, 0f, 1f);

    Texture texture;

    float energyLevel;
    float coldLevel;
    float lifeLevel;

    public StatusBar() {
        texture = TextureManager.statusBarTexture;
        setX(0);
        setY(GameScreen.WORLD_HEIGHT - height);
        texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
    }

    public void draw(Batch batch) {
        batch.setColor(foodSecColor);
        batch.draw(texture, getX(), getY(), width, height);
        batch.setColor(foodColor);
        batch.draw(texture, getX(), getY(), energyLevel, height);
        batch.setColor(coldSecColor);
        batch.draw(texture, getX(), getY() - height, width, height);
        batch.setColor(coldColor);
        batch.draw(texture, getX(), getY() - height, coldLevel, height);
        batch.setColor(lifeSecColor);
        batch.draw(texture, getX(), getY() - height * 2, width, height);
        batch.setColor(lifeColor);
        batch.draw(texture, getX(), getY() - height * 2, lifeLevel, height);
    }

    public void setEnergyLevel(float energyLevel1){
        energyLevel = energyLevel1;
    }

    public void setColdLevel(float coldLevel1){
        coldLevel = coldLevel1;
    }

    public void setLifeLevel(float lifeLevel) {
        this.lifeLevel = lifeLevel;
    }

    public float getLifeLevel() {
        return lifeLevel;
    }
}
