package com.vdroog1.ludumdare31;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LudumGame extends Game {

    public SpriteBatch batch;

	@Override
	public void create () {
        TextureManager.load();
		batch = new SpriteBatch();
        GameScreen gameScreen = new GameScreen(this);
        setScreen(gameScreen);
	}

	@Override
	public void render () {
		super.render();
	}

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
        TextureManager.dispose();
    }
}
