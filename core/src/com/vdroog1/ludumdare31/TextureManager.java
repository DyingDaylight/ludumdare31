package com.vdroog1.ludumdare31;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by kettricken on 08.12.2014.
 */
public class TextureManager {

    public static Texture food  = new Texture( Gdx.files.internal("food.png"));
    public static Texture player  = new Texture( Gdx.files.internal("player.png"));
    public static Texture bot  = new Texture( Gdx.files.internal("bot.png"));
    public static Texture fire  = new Texture( Gdx.files.internal("fire.png"));
    public static Texture flame  = new Texture( Gdx.files.internal("flame.png"));
    public static TextureRegion flameRegion;
    public static Texture statusBarTexture;
    public static Texture rules;
    public static Texture lostRip;
    public static Texture lostFood;
    public static Texture lostCold;
    public static Texture win;
    public static Texture burningPlayer;

    public static void load() {
        food  = new Texture( Gdx.files.internal("food.png"));
        player  = new Texture( Gdx.files.internal("player.png"));
        bot  = new Texture( Gdx.files.internal("bot.png"));
        fire  = new Texture( Gdx.files.internal("fire.png"));
        flame  = new Texture( Gdx.files.internal("flame.png"));
        rules  = new Texture( Gdx.files.internal("rule.png"));
        lostRip  = new Texture( Gdx.files.internal("game_over_dead.png"));
        lostFood  = new Texture( Gdx.files.internal("game_over_starved.png"));
        lostCold  = new Texture( Gdx.files.internal("game_over_frozen.png"));
        burningPlayer  = new Texture( Gdx.files.internal("burning_player.png"));
        win  = new Texture( Gdx.files.internal("win.png"));
        flame.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        flameRegion = new TextureRegion(flame);
        Pixmap pixmap = new Pixmap(80, 16, Pixmap.Format.RGBA8888);
        pixmap.setColor(1, 1, 1, 1);
        pixmap.fill();
        statusBarTexture = new Texture(pixmap);
        pixmap.dispose();
    }

    public static void dispose() {
        food.dispose();
        player.dispose();
        bot.dispose();
        fire.dispose();

       flame.dispose();
        statusBarTexture.dispose();
       rules.dispose();
        lostRip.dispose();
         lostFood.dispose();
       lostCold.dispose();
          win.dispose();
        burningPlayer.dispose();
    }
}
