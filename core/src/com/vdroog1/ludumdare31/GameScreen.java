package com.vdroog1.ludumdare31;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;

/**
 * Created by kettricken on 06.12.2014.
 */
public class GameScreen implements Screen, InputProcessor {

    // ------- sizes ------------
    public static final int WORLD_WITH = 640;
    public static final int WORLD_HEIGHT = 380;
    public static final int LIGHT_HALF_SIZE = 25;


    // ------ time --------
    public static final int FOOD_GENERATION_INTERVAL = 1 * 10 * 1000;


    // ------ textures --------
    private final TextureRegion fog;
    private final TextureRegion see;
    private final TextureRegion ashMask;
    final Pixmap pixmap;
    Texture mapTexture;
    Texture ripleyTexture;

    int botsNum = 4;

    Clock clock = new Clock();

    OrthographicCamera camera;
    LudumGame game;

    long lastFoodTime = 0;
    float ripleyShrinkTime = 0;

    Array<Food> food = new Array<Food>();

    Player player;
    ShapeRenderer shapeRenderer;
    Util util;

    PolygonSpriteBatch polygonSpriteBatch;

    Array<Fire> fireRecta = new Array<Fire>();
    Array<Ashes> asheses = new Array<Ashes>();

    TextureRegion ashesTexture;

    House house = new House();

    Array<Player> bots = new Array<Player>();

    AILogic logic;

    private boolean isGameStarted = false;
    private boolean gameOver = false;

    public GameScreen(LudumGame game) {
        this.game = game;
        util = new Util();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, WORLD_WITH, WORLD_HEIGHT);

        player = new Player(false, util);
        bots.add(player);

        for (int i = 0; i < botsNum; i ++) {
            Player player1 = new Player(true, util);
            player1.cold = 7;
            bots.add(player1);
        }

        shapeRenderer = new ShapeRenderer();

        FileHandle dataFile = Gdx.files.internal("map.png");
        Pixmap tmppixmap = new Pixmap(dataFile);
        mapTexture = new Texture(tmppixmap);
        pixmap = flipPixmap(tmppixmap);

        ripleyTexture = new Texture( Gdx.files.internal("ripley.png"));

        Gdx.input.setInputProcessor(this);

        polygonSpriteBatch = new PolygonSpriteBatch();

        ashesTexture = new TextureRegion(new Texture(Gdx.files.internal("ashes.png"))/*, -Constants.mapOffset, -Constants.mapOffset,
                WORLD_WITH, WORLD_HEIGHT*/);

        Texture texture = new Texture(Gdx.files.internal("seeit.png"));
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        see = new TextureRegion(texture);
        ashMask = new TextureRegion(new Texture(Gdx.files.internal("ash_mask.png")));
        fog = new TextureRegion(new Texture(Gdx.files.internal("fog.png")));

        logic = new AILogic(bots, fireRecta, food, util, pixmap);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);
        polygonSpriteBatch.setProjectionMatrix(camera.combined);


        if (clock.daysPassed == clock.daysRequired){
            gameOver = true;
            game.batch.begin();
            game.batch.setColor(1, 1, 1, 1);
            game.batch.draw(TextureManager.win, 200, 134);
            game.batch.end();
            return;
        } else if (!isGameStarted) {
            game.batch.begin();
            game.batch.setColor(1, 1, 1, 1);
            game.batch.draw(TextureManager.rules, 50, 50);
            game.batch.end();
            return;
        } else if (player.life <= 0) {
            gameOver = true;
            game.batch.begin();
            game.batch.setColor(1, 1, 1, 1);
            game.batch.draw(TextureManager.lostRip, 100, 134);
            game.batch.end();
            return;
        } else if (player.energy == 0) {
            gameOver = true;
            game.batch.begin();
            game.batch.setColor(1, 1, 1, 1);
            game.batch.draw(TextureManager.lostFood, 200, 134);
            game.batch.end();
            return;
        } else if (player.cold == 100) {
            gameOver = true;
            game.batch.begin();
            game.batch.setColor(1, 1, 1, 1);
            game.batch.draw(TextureManager.lostCold, 200, 134);
            game.batch.end();
            return;
        }

        logic.update(delta);
        Iterator<Player> playerIterator = bots.iterator();
        while (playerIterator.hasNext()) {
            Player player1 = playerIterator.next();
            player1.update(pixmap, delta);
            player1.testPlayerForFire(fireRecta, delta);
            player1.testForFood(food);
            player1.checkForVirus(util);
            player1.checkToBurn(fireRecta, pixmap);
            player1.chechFriendlyFireAndContect(bots);
            if (player1.isBot && player1.energy == 0 || player1.life == 0
                    || player1.cold == 100){
                playerIterator.remove();
            }
        }

        if (TimeUtils.millis() - lastFoodTime > FOOD_GENERATION_INTERVAL && !house.isOnFire) {
            lastFoodTime = TimeUtils.millis();
            Food food1 = new Food();
            food.add(food1);
        }
        ripleyShrinkTime += delta;

        if (ripleyShrinkTime >= 0.5f) {
            util.generatePolygon();
            ripleyShrinkTime = 0;
        }

        clock.act(delta);

        game.batch.begin();
        game.batch.setColor(1, 1, 1, 1);
        game.batch.draw(ripleyTexture, -Constants.mapOffset, -Constants.mapOffset);
        game.batch.end();

        polygonSpriteBatch.begin();
        polygonSpriteBatch.setColor(1,1,1,1);
        polygonSpriteBatch.draw(util.mapRegion, 0f, 0f);
        polygonSpriteBatch.end();

        drawAshes();

        game.batch.begin();
        for (Player player1 : bots){
            if (player1.isContagious)
                game.batch.setColor(1, 0, 0, 1);
            else
                game.batch.setColor(1, 1, 1, 1);
            if (player1.isOnFire)
                game.batch.draw(TextureManager.burningPlayer, player1.x, player1.y, Player.playerHeight, Player.playerHeight );
            else
                game.batch.draw(player1.texture,player1.x, player1.y, Player.playerHeight, Player.playerHeight );
        }

        Iterator<Fire> fireIterator = fireRecta.iterator();
        while (fireIterator.hasNext()) {
            Fire fire = fireIterator.next();
            if (fire.isValid()) {
                game.batch.setColor(1,1,1,1);
                game.batch.draw(fire.texture, fire.rectangle.x, fire.rectangle.y, fire.rectangle.width, fire.rectangle.height);
                if (Intersector.intersectRectangles(fire.rectangle, house.rectangle, new Rectangle())) {
                    house.setOnFire();
                }
            } else {
                Ashes ashes = new Ashes(fire.rectangle);
                asheses.add(ashes);
                fireIterator.remove();
            }
        }

        Iterator<Food> foodIterator = food.iterator();
        while (foodIterator.hasNext()) {
            Food food = foodIterator.next();
            if (food.isValid()) {
                game.batch.setColor(1,1,1,1);
                game.batch.draw(TextureManager.food, food.rectangle.x, food.rectangle.y, food.rectangle.width, food.rectangle.height);
            } else {
                foodIterator.remove();
            }
        }
        game.batch.end();

        polygonSpriteBatch.begin();
        for (Player player1 : bots) {
            polygonSpriteBatch.setColor(1,1,1,1);
            player1.drawFire(polygonSpriteBatch);
        }
        polygonSpriteBatch.end();

        if (!clock.isDayTime) {
            drawNightTest();
        }

        game.batch.begin();
      //  player.draw(game.batch);
        for (Player player1 : bots) {

            player1.draw(game.batch);
        }
        game.batch.end();
        clock.draw(shapeRenderer, game.batch);
    }

    private boolean drawNightTest() {
        if (pixmap == null)
            return false;

        Gdx.gl.glClear(GL20.GL_STENCIL_BUFFER_BIT);
        game.batch.begin();
        Gdx.gl.glColorMask(false, false, false, false);
        Gdx.gl.glDepthMask(false);
        Gdx.gl.glEnable(GL20.GL_STENCIL_TEST);
        Gdx.gl.glStencilFunc(GL20.GL_ALWAYS, 0x1, 0xffffffff);
        Gdx.gl.glStencilOp(GL20.GL_REPLACE, GL20.GL_REPLACE, GL20.GL_REPLACE);
        for (Fire fire : fireRecta) {
            game.batch.draw(see, fire.getCenter().x - LIGHT_HALF_SIZE, fire.getCenter().y - LIGHT_HALF_SIZE);
        }
        player.drawNightFires(game.batch, see);
        for (Player player1 : bots) {
            player1.drawNightFires(game.batch, see);
        }

        game.batch.end();
        game.batch.begin();

        Gdx.gl.glColorMask(true, true, true, true);
        Gdx.gl.glDepthMask(true);
        Gdx.gl.glStencilFunc(GL20.GL_NOTEQUAL, 0x1, 0xffffffff);
        Gdx.gl.glStencilOp(GL20.GL_KEEP, GL20.GL_KEEP, GL20.GL_KEEP);
        game.batch.setColor(1,1,1,0.9f);
        game.batch.draw(fog, 0, 0);
        game.batch.end();
        Gdx.gl.glDisable(GL20.GL_STENCIL_TEST);

        return true;
    }


    private boolean drawAshes() {
        Gdx.gl.glClear(GL20.GL_STENCIL_BUFFER_BIT);
        game.batch.begin();
        Gdx.gl.glColorMask(false, false, false, false);
        Gdx.gl.glDepthMask(false);
        Gdx.gl.glEnable(GL20.GL_STENCIL_TEST);
        Gdx.gl.glStencilFunc(GL20.GL_ALWAYS, 0x1, 0xffffffff);
        Gdx.gl.glStencilOp(GL20.GL_REPLACE, GL20.GL_REPLACE, GL20.GL_REPLACE);
        house.setOffFire();
        Iterator<Ashes> ashesIterator = asheses.iterator();
        while (ashesIterator.hasNext()) {
            Ashes ashes1 = ashesIterator.next();
            if (ashes1.isValid()) {
                game.batch.draw(ashMask, ashes1.rectangle.x, (int) ashes1.rectangle.y);
                if (Intersector.intersectRectangles(ashes1.rectangle, house.rectangle, new Rectangle())) {
                    house.setOnFire();
                }
            } else {
                ashesIterator.remove();
            }
        }
        game.batch.end();
        game.batch.begin();

        Gdx.gl.glColorMask(true, true, true, true);
        Gdx.gl.glDepthMask(true);
        Gdx.gl.glStencilFunc(GL20.GL_EQUAL, 0x1, 0xffffffff);
        Gdx.gl.glStencilOp(GL20.GL_KEEP, GL20.GL_KEEP, GL20.GL_KEEP);
        game.batch.setColor(1, 1, 1, 1f);
        game.batch.draw(ashesTexture, 0, 0);
        game.batch.end();
        Gdx.gl.glDisable(GL20.GL_STENCIL_TEST);

        return true;
    }
    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode)
        {
            case Input.Keys.SPACE:
                //util.generatePolygon();
                break;
        }
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (isGameStarted) {
            Vector3 vector3 = new Vector3(screenX, screenY, 0);
            camera.unproject(vector3);
            if (button == Input.Buttons.LEFT) {
                player.startFire((int) vector3.x, (int) vector3.y);
                //logic.setTestPoint((int) vector3.x, (int) vector3.y);
            }
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (isGameStarted && !gameOver) {
            player.stopFire();
        } else if (!isGameStarted) {
            isGameStarted = true;
        } else if (gameOver) {
            restart();
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (isGameStarted) {
            Vector3 vector3 = new Vector3(screenX, screenY, 0);
            camera.unproject(vector3);
            player.startFire((int) vector3.x, (int) vector3.y);
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public Pixmap flipPixmap(Pixmap src) {
        final int width = src.getWidth();
        final int height = src.getHeight();
        Pixmap flipped = new Pixmap(width, height, src.getFormat());

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                flipped.drawPixel(x, y, src.getPixel(x, height - y -1));
            }
        }
        return flipped;
    }

    public void restart() {
        bots.clear();
        player = new Player(false, util);
        bots.add(player);
        for (int i = 0; i < botsNum; i ++) {
            Player player1 = new Player(true, util);
            bots.add(player1);
        }
        fireRecta.clear();
        asheses.clear();
        food.clear();
        util.restartRipley();

        lastFoodTime = 0;
        ripleyShrinkTime = 0;
        clock.restart();
        gameOver = false;
        isGameStarted = false;
    }
}
