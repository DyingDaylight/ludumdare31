package com.vdroog1.ludumdare31;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Created by kettricken on 07.12.2014.
 */
public class Fire {

    private static final long LIFETIME = 1000 * 5;
    private static final int WIDTH_HEIGHT = 32;

    Rectangle rectangle;

    long timestamp;

    Vector2 center = new Vector2();

    Texture texture;

    public Fire(int x, int y) {
        timestamp = TimeUtils.millis();
        rectangle = new Rectangle(x - WIDTH_HEIGHT / 2, y - WIDTH_HEIGHT / 2,
                WIDTH_HEIGHT, WIDTH_HEIGHT);
        texture = TextureManager.fire;
    }

    public boolean isValid(){
        if (TimeUtils.millis() - timestamp > LIFETIME)
            return false;
        return true;
    }

    public Vector2 getCenter(){
        center.x = rectangle.x + WIDTH_HEIGHT / 2;
        center.y = rectangle.y + WIDTH_HEIGHT / 2;
        return center;
    }
}
