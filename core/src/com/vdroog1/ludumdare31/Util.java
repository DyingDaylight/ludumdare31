package com.vdroog1.ludumdare31;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ShortArray;

import java.util.ArrayList;

/**
 * Created by kettricken on 06.12.2014.
 */
public class Util {

    public Polygon ripley = new Polygon();
    int width = GameScreen.WORLD_WITH;
    int height = GameScreen.WORLD_HEIGHT;
    Vector2 center = new Vector2(width/ 2, height / 2);
    float dif = 30f;
    float movement = 2f;
    float movementBack = 10f;

    PolygonRegion mapRegion;

    TextureRegion mapTextureRegion;
    Texture mapTexture;

    TextureRegion test = new TextureRegion();
    Pixmap pix = new Pixmap(width, height, Pixmap.Format.RGBA8888);
    EarClippingTriangulator earClippingTriangulator = new EarClippingTriangulator();
    ShortArray triangles;
    static Vector2 pointOnVector = new Vector2();
    Vector2 currentPoint = new Vector2();
    public Util() {

        FileHandle dataFile = Gdx.files.internal("map-drawing.png");
        mapTexture = new Texture(dataFile);
        mapTextureRegion = new TextureRegion(mapTexture, Constants.mapOffset, Constants.mapOffset, width, height);
        generateNewRipley();

    }

    private void generateNewRipley() {
        ArrayList<Float> vertices = new ArrayList<Float>();

        float x = 0;
        float y = 0;
        for (x = 0; x < width; x += dif) {
            vertices.add(x);
            vertices.add(0f);
        }

        for (y = 0; y < height; y += dif) {
            vertices.add(x);
            vertices.add(y);
        }

        for (x = x; x > 0; x -= dif) {
            vertices.add(x);
            vertices.add(y);
        }

        for (y = y; y > 0; y -= dif) {
            vertices.add(x);
            vertices.add(y);
        }
        float[] stockArr = new float[vertices.size()];
        int p = 0;
        for (Float f : vertices) {
            stockArr[p++] = (f != null ? f : Float.NaN); // Or whatever default you want.
        }
        ripley.setVertices(stockArr);
        triangles = earClippingTriangulator.computeTriangles(ripley.getVertices());
        mapRegion = new PolygonRegion(mapTextureRegion, ripley.getVertices(), triangles.items);
        pix.setColor(0, 0, 0, 1);
        pix.fillRectangle(0, 0, width,height);
        pix.setColor(1,1,1,1);
        for (int i = 0; i < triangles.size; i += 3) {
            int p1 = triangles.get(i) * 2;
            int p2 = triangles.get(i + 1) * 2;
            int p3 = triangles.get(i + 2) * 2;
            pix.fillTriangle( //
                    (int) ripley.getVertices()[p1], (int) ripley.getVertices()[p1 + 1], //
                    (int) ripley.getVertices()[p2], (int) ripley.getVertices()[p2 + 1], //
                    (int) ripley.getVertices()[p3], (int) ripley.getVertices()[p3 + 1] //
            );
        }


        test.setRegion(new Texture(pix));
    }

    int[] indicesToChange = new int[0];
    public void generatePolygon() {
        int num = ripley.getVertices().length / 2;
        indicesToChange = new int[num];
        for (int i = 0; i < num; i++) {
            indicesToChange[i] = MathUtils.random(0, ripley.getVertices().length - 1);
        }
        for (int i = 0; i < num; i++) {
            int rest = indicesToChange[i] %2;
            float d = MathUtils.random(0, movement);
            if (rest == 0) {
                currentPoint.x = ripley.getVertices()[indicesToChange[i]];
                currentPoint.y = ripley.getVertices()[indicesToChange[i] + 1];
            } else {
                currentPoint.y = ripley.getVertices()[indicesToChange[i]];
                currentPoint.x = ripley.getVertices()[indicesToChange[i] - 1];
            }

            float diff = (float) Math.sqrt(Math.pow(currentPoint.x - center.x, 2) + Math.pow(currentPoint.y - center.y, 2));
            currentPoint.x = currentPoint.x + d / diff * (center.x - currentPoint.x);
            currentPoint.y = currentPoint.y + d / diff * (center.y - currentPoint.y);

            if (rest == 0) {
                ripley.getVertices()[indicesToChange[i]] = currentPoint.x;
                ripley.getVertices()[indicesToChange[i] + 1] = currentPoint.y;
            } else {
                ripley.getVertices()[indicesToChange[i]] = currentPoint.y;
                ripley.getVertices()[indicesToChange[i] - 1] = currentPoint.x;
            }
        }

        triangles = earClippingTriangulator.computeTriangles(ripley.getVertices());
        mapRegion = new PolygonRegion(mapTextureRegion, ripley.getVertices(), triangles.items);

        pix.setColor(0, 0, 0, 1);
        pix.fillRectangle(0, 0, width,height);
        pix.setColor(1,1,1,1);
        for (int i = 0; i < triangles.size; i += 3) {
            int p1 = triangles.get(i) * 2;
            int p2 = triangles.get(i + 1) * 2;
            int p3 = triangles.get(i + 2) * 2;
            pix.fillTriangle( //
                    (int) ripley.getVertices()[p1], (int) ripley.getVertices()[p1 + 1], //
                    (int) ripley.getVertices()[p2], (int) ripley.getVertices()[p2 + 1], //
                    (int) ripley.getVertices()[p3], (int) ripley.getVertices()[p3 + 1] //
            );
        }

        test.setRegion(new Texture(pix));
    }

    public static Vector2 getPointOnLine(Vector2 center, Vector2 aim, float d){
        float diff = (float) Math.sqrt(Math.pow(aim.x - center.x, 2) + Math.pow(aim.y - center.y, 2));
        pointOnVector.x = aim.x + d / diff * (center.x - aim.x);
        pointOnVector.y = aim.y + d / diff * (center.y - aim.y);
        return pointOnVector;
    }

    public void removeRipley(Array<Vector2> polygon) {
        for (int i = 0; i < ripley.getVertices().length; i += 2) {
            if (Intersector.isPointInPolygon(polygon, new Vector2(ripley.getVertices()[i],
                    ripley.getVertices()[i+1]))){
                float diff = (float) Math.sqrt(Math.pow(ripley.getVertices()[i] - center.x, 2) +
                        Math.pow(ripley.getVertices()[i+1] - center.y, 2));
                float d = MathUtils.random(0, movementBack);
                ripley.getVertices()[i] = ripley.getVertices()[i] - d / diff * (center.x - ripley.getVertices()[i]);
                ripley.getVertices()[i+1] =ripley.getVertices()[i+1] - d / diff * (center.y - ripley.getVertices()[i+1]);
            }
        }
        triangles = earClippingTriangulator.computeTriangles(ripley.getVertices());
        mapRegion = new PolygonRegion(mapTextureRegion, ripley.getVertices(), triangles.items);

        pix.setColor(0, 0, 0, 1);
        pix.fillRectangle(0, 0, width,height);
        pix.setColor(1,1,1,1);
        for (int i = 0; i < triangles.size; i += 3) {
            int p1 = triangles.get(i) * 2;
            int p2 = triangles.get(i + 1) * 2;
            int p3 = triangles.get(i + 2) * 2;
            pix.fillTriangle( //
                    (int) ripley.getVertices()[p1], (int) ripley.getVertices()[p1 + 1], //
                    (int) ripley.getVertices()[p2], (int) ripley.getVertices()[p2 + 1], //
                    (int) ripley.getVertices()[p3], (int) ripley.getVertices()[p3 + 1] //
            );
        }

        test.setRegion(new Texture(pix));

    }

    public void restartRipley() {
        generateNewRipley();
    }
}
